package mk.ukim.finki.emt.deviceshop.repository.jpa;

import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaManufacturerRepository extends JpaRepository<Manufacturer,Long> {
}
