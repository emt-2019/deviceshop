package mk.ukim.finki.emt.deviceshop.service;

import mk.ukim.finki.emt.deviceshop.models.Manufacturer;

import java.util.List;
import java.util.Optional;

public interface ManufacturerService {
    List<Manufacturer> getAll();
    Optional<Manufacturer> getOne(Long id);
}
