package mk.ukim.finki.emt.deviceshop.service.impl;

import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.repository.ManufacturerRepository;
import mk.ukim.finki.emt.deviceshop.service.ManufacturerService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile("in-memory")
public class ManufacturerServiceImpl implements ManufacturerService {

    private ManufacturerRepository repo;

    public ManufacturerServiceImpl(ManufacturerRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Manufacturer> getAll() {
        return repo.findAll();
    }

    @Override
    public Optional<Manufacturer> getOne(Long id) {
        return getAll().stream().filter(v->id.equals(v.getId())).findAny();
    }


}
