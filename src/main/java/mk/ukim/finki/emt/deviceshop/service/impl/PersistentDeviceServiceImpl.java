package mk.ukim.finki.emt.deviceshop.service.impl;

import mk.ukim.finki.emt.deviceshop.exceptions.DeviceNotFoundException;
import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.repository.PersistentDeviceRepository;
import mk.ukim.finki.emt.deviceshop.repository.jpa.JpaDeviceRepository;
import mk.ukim.finki.emt.deviceshop.service.DeviceService;
import mk.ukim.finki.emt.deviceshop.service.ManufacturerService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile("persist")
public class PersistentDeviceServiceImpl implements DeviceService {

    private JpaDeviceRepository repo;
    private ManufacturerService manufacturerService;

    public PersistentDeviceServiceImpl(JpaDeviceRepository repo, ManufacturerService manufacturerService) {
        this.repo = repo;
        this.manufacturerService = manufacturerService;
    }

    @Override
    public Device addNewDevice(Device device, Long manufacturerId, String username) {
        Optional<Manufacturer> man = manufacturerService.getAll().stream().filter(v -> v.getId().equals(manufacturerId)).findAny();
        if (!man.isPresent()) {
            throw new ManufacturerNotFoundException();
        }
        device.setManufacturer(man.get());
        return repo.save(device);
    }

    @Override
    public Device addNewDevice(String name, Double price, Long manufacturerId) throws ManufacturerNotFoundException {
        return null;
    }

    @Override
    public Device addNewDevice(String name, Double price, Long manufacturerId, byte[] photo) throws ManufacturerNotFoundException {
        Device d = new Device();
        d.setName(name);
        d.setPrice(price);
        d.setPhoto(photo);
        d.setManufacturer(manufacturerService.getOne(manufacturerId).get());
        return repo.save(d);
    }

    @Override
    public Device addNewDevice(Device device, String username) {
        return null;
    }

    @Override
    public List<Device> getAllDevices() {
        return repo.findAll();
    }

    @Override
    public Device update(Device device) throws DeviceNotFoundException {
        return null;
    }

    @Override
    public void delete(Long deviceId) {
        repo.deleteById(deviceId);
    }

    @Override
    public Device getById(Long deviceId) {
        return repo.findById(deviceId).orElseThrow(()->new DeviceNotFoundException());
    }

    @Override
    public List<Device> getByManId(Long manId) {
        return repo.findByManufacturerId(manId);
    }
}
