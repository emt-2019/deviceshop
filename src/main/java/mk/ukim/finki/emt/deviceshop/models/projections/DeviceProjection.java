package mk.ukim.finki.emt.deviceshop.models.projections;

public interface DeviceProjection {
    Long getId();
    String getName();

}
