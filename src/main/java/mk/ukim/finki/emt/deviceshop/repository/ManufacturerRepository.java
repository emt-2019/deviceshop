package mk.ukim.finki.emt.deviceshop.repository;

import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ManufacturerRepository {

    private Long counter = 0L;
    private List<Manufacturer> manufacturers = null;

    @PostConstruct
    public void postConstruct() {

        manufacturers = new ArrayList<>();
        Manufacturer m = new Manufacturer();
        m.setId(getNextId());
        m.setName("Samsung");
        manufacturers.add(m);

    }

    public List<Manufacturer> findAll() {
        return manufacturers;
    }

    private Long getNextId() {
        return counter++;
    }
}
