package mk.ukim.finki.emt.deviceshop.repository;

import mk.ukim.finki.emt.deviceshop.models.Category;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.models.projections.DeviceProjection;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PersistentDeviceRepository extends Repository<Device,Long> {

    @Query("select d from Device d")
    List<Device> getAll();

    Device save(Device d);

    @Query(value="delete from Device d where d=:d")
    void delete(@Param("d") Device d);

    @Modifying
    @Transactional
    @Query(value="delete from Device d where d.id=:id")
    void delete(@Param("id") Long id);

    @Query("select d from Device d where d.id=:id")
    Optional<Device> getById(@Param("id") Long id);

    @Query("select d from Device d where d.category=:category")
    List<Device> getByCategory(@Param("category") Category category);

    @Query("select d from Device d where d.category=:category and d.manufacturer=:manufacturer")
    List<Device> getByCategoryAndManufacturer(@Param("category") Category category, @Param("manufacturer")Manufacturer manufacturer);

    @Query("select d from Device d order by d.name")
    List<Device> getAllOrderByName(@Param("category") Category category);

    @Query(value = "select * from device d left join category c on d.cat_id=c.id",nativeQuery = true)
    List<Device> getAllWithNativeQuery(@Param("category") Category category);

    @Query(value="select d from Device d where d.dateProduced>:dateProduced")
    List<Device> getNewerDevices(@Param("dateProduced")LocalDateTime dateProduced);

    @Query(value="select d from Device d where d.id in :idList")
    List<Device> getAllDevicesByIds(@Param("idList") List<Long> ids);

    @Query(value="select count(d.id) from Device d")
    Long countItems();

    @Query(value="select sum(d.price) from Device d where d.category=:category")
    Double calculatePrice(@Param("category") Category category);

    @Query("select d from Device d where d.manufacturer=:manId")
    List<Device> getByManufacturerId(@Param("man") Manufacturer man);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value="update  Device d set d.name=:name where d.id=:id")
    Device updateByName(@Param("id") Long id,@Param("name") String name);

}
