package mk.ukim.finki.emt.deviceshop.repository.jpa;

import mk.ukim.finki.emt.deviceshop.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long>{

    User findByUsername(String username);
}
