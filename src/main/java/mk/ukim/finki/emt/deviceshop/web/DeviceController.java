package mk.ukim.finki.emt.deviceshop.web;

import mk.ukim.finki.emt.deviceshop.exceptions.ManufacturerNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.models.User;
import mk.ukim.finki.emt.deviceshop.service.DeviceService;
import mk.ukim.finki.emt.deviceshop.service.ManufacturerService;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.plugin.liveconnect.SecurityContextHelper;
import sun.plugin2.applet.SecurityManagerHelper;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/device")
public class DeviceController {

    private DeviceService deviceService;
    private ManufacturerService manufacturerService;

    public DeviceController(DeviceService service,ManufacturerService manufacturerService) {
        this.deviceService = service;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping("/")
    public String devices(Model model) {
        model.addAttribute("deviceList",deviceService.getAllDevices());
        model.addAttribute("manufacturerList",manufacturerService.getAll());
        model.addAttribute("device", new Device());
        return "devices.form";
    }

    @ExceptionHandler({ManufacturerNotFoundException.class})
    @PostMapping("/")
    public String addDevice(HttpServletRequest request, Model model) {
        String name = request.getParameter("name");
        Double price = Double.parseDouble(request.getParameter("price"));
        Long manId = Long.parseLong(request.getParameter("manufacturerId"));
        deviceService.addNewDevice(name,price,manId);
        return "redirect:/device/";

    }

    @ExceptionHandler({ManufacturerNotFoundException.class})
    @PostMapping("/add")
    public String addDeviceWithModelAttribute(@ModelAttribute Device d, Model model,
                                              @RequestParam("image") MultipartFile file) throws IOException {

        deviceService.addNewDevice(d.getName(),d.getPrice(),d.getManufacturer().getId(),file.getBytes());
        return "redirect:/device/";
    }
    @DeleteMapping("/")
    @PreAuthorize("isAuthenticated() && hasRole('ROLE_ADMIN')")
    public String deleteDevice(HttpServletRequest request) {
        Long deviceId = Long.parseLong(request.getParameter("deviceId"));
        deviceService.delete(deviceId);
        return "redirect:/device/";
    }

    @RequestMapping(value = "/image/{device_id}")
    public ResponseEntity<byte[]> getImage(@PathVariable("device_id") Long deviceId) throws IOException {

        Device device = deviceService.getById(deviceId);
        byte[] imageContent = device.getPhoto();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDisposition(ContentDisposition.parse("attachment; filename="+ device.getName()));
        headers.setContentLength(imageContent.length);
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(imageContent.length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(imageContent);
    }

}
