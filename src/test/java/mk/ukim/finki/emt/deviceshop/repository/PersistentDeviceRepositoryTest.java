package mk.ukim.finki.emt.deviceshop.repository;

import mk.ukim.finki.emt.deviceshop.exceptions.DeviceNotFoundException;
import mk.ukim.finki.emt.deviceshop.models.Category;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.projections.DeviceProjection;
import mk.ukim.finki.emt.deviceshop.repository.jpa.JpaDeviceRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PersistentDeviceRepositoryTest {

    @Autowired
    private JpaDeviceRepository repo;

    @Autowired
    private PersistentCategoryRepository categoryRepository;

    @Before
    public void init() {
        Category c = new Category();
        c.setName("phones");
        categoryRepository.save(c);

        Device d1 = new Device();
        d1.setName("SAMSUNG");
        d1.setPrice(10d);
        d1.setCategory(c);
        repo.save(d1);

        Device d2 = new Device();
        d2.setName("LG");
        d2.setPrice(20d);
        d2.setCategory(c);
        repo.save(d2);
    }

    @Test
    public void getAll() {
        List<Device> deviceList = repo.findAll();
        Assert.assertEquals(2,deviceList.size());
    }

    @Test
    public void getById() {
        Optional<Device> optD = repo.findById(1L);
        Assert.assertNotNull(optD.get());
        Assert.assertEquals("SAMSUNG",optD.get().getName());
    }

    @Test
    public void testProjection() {
//        List<DeviceProjection> deviceList = repo.findBy();
//        Assert.assertEquals(2l,deviceList.size());
    }



    @Test
    public void save() {
    }

    @Test
    public void delete() {
    }



    @Test
    public void getByCategory() {
    }

    @Test
    public void getByCategoryAndManufacturer() {
    }

    @Test
    public void getAllOrderByName() {
    }



    @Test
    public void getNewerDevices() {
    }

    @Test
    public void getAllDevicesByIds() {
    }

    @Test
    public void countItems() {
        long i = repo.count();
        Assert.assertEquals(2L,i);
    }




    @Test
    public void updateByName() {
        Device d = repo.findById(1L).orElseThrow(()->new DeviceNotFoundException());
        d.setName("iPhone");
        repo.save(d);
        Optional<Device> dNew = repo.findById(1L);
        Assert.assertEquals("iPhone",dNew.get().getName());

    }
}