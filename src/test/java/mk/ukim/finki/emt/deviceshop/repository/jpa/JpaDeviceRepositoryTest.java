package mk.ukim.finki.emt.deviceshop.repository.jpa;

import mk.ukim.finki.emt.deviceshop.dto.DeviceReduced;
import mk.ukim.finki.emt.deviceshop.models.Category;
import mk.ukim.finki.emt.deviceshop.models.Device;
import mk.ukim.finki.emt.deviceshop.models.Manufacturer;
import mk.ukim.finki.emt.deviceshop.models.projections.DeviceProjection;
import mk.ukim.finki.emt.deviceshop.repository.ManufacturerRepository;
import mk.ukim.finki.emt.deviceshop.repository.PersistentCategoryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JpaDeviceRepositoryTest {

    @Autowired
    private JpaDeviceRepository repo;

    @Autowired
    private PersistentCategoryRepository categoryRepository;

    @Autowired
    private JpaManufacturerRepository manufacturerRepository;

    @Before
    public void init() {
        Category c = new Category();
        c.setName("phones");
        categoryRepository.save(c);

        Manufacturer m = new Manufacturer();
        m.setName("SAMSUNG");
        manufacturerRepository.save(m);

        Device d1 = new Device();
        d1.setName("Galaxy");
        d1.setPrice(10d);
        d1.setCategory(c);
        d1.setManufacturer(m);
        repo.save(d1);

        Device d2 = new Device();
        d2.setName("Y6");
        d2.setPrice(20d);
        d2.setCategory(c);
        d2.setManufacturer(m);
        repo.save(d2);
    }

    @Test
    public void findByCategoryId() {
        List<Device> deviceList = repo.findByCategoryId(1L);
        Assert.assertEquals(2L,deviceList.size());
    }

    @Test
    public void findByCategoryIdAndManufacturerId() {
    }

    @Test
    public void count() {
    }

    @Test
    public void countByCategoryId() {
    }

    @Test
    public void findByCategoryIdOrManufacturerId() {
    }

    @Test
    public void findByManufacturerNameLike() {
    }

    @Test
    public void findBy() {
        List<DeviceProjection> deviceList = repo.findBy();
        Assert.assertEquals(2L,deviceList.size());
    }
}